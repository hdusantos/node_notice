module.exports = function(app) {
    app.get('/noticia', function(requisicao, resposta) {
        const conexao = app.config.dbConnection();
        const consulta = new app.app.models.NoticiasModel(conexao);

        consulta.getNoticia(function(erro, resultado) {
            resposta.render('noticias/noticia', {noticia: resultado});
        });
    });
};
