module.exports = function(app) {
    app.get('/noticias', function(requisicao, resposta) {
        const conexao = app.config.dbConnection();
        const noticiasModel = new app.app.models.NoticiasModel(conexao);

        noticiasModel.getNoticias(function(erro, resultado) {
            resposta.render('noticias/noticias', {noticias: resultado});
        });
    });
};
