const mysql = require('mysql');

const databaseConnect = function() {
    return mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: 'madara314',
        database: 'portal_noticias',
    });
};

module.exports = function() {
    return databaseConnect;
};
