const express = require('express');
const consign = require('consign');
const bodyParser = require('body-parser');

const app = express();

app.set('view engine', 'ejs');
app.set('views', './app/views');

app.use(bodyParser.urlencoded({extended: true}));

// Definindo pasta das rotas, db e setando em apps
consign()
    .include('app/routes')
    .then('app/models')
    .then('config/dbConnection.js')
    .into(app);

module.exports = app;
